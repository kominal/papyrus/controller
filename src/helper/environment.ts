import { getEnvironmentString } from '@kominal/lib-node-environment';

export const CORE_BASE_URL = getEnvironmentString('CORE_BASE_URL', 'core.kominal.app');
export const CORE_SERVICE_TOKEN = getEnvironmentString('CORE_SERVICE_TOKEN');
