import { ExpressRouter } from '@kominal/lib-node-express-router';
import { MongoDBInterface } from '@kominal/lib-node-mongodb-interface';
import { info, startObserver } from '@kominal/observer-node-client';
import { projectRouter } from './routes/project';
import { tenantsRouter } from './routes/tenants';
import { translationRouter } from './routes/translation';

let mongoDBInterface: MongoDBInterface;
let expressRouter: ExpressRouter;

async function start() {
	startObserver();
	mongoDBInterface = new MongoDBInterface('controller');
	await mongoDBInterface.connect();
	expressRouter = new ExpressRouter({
		baseUrl: 'controller',
		healthCheck: async () => true,
		routes: [translationRouter, projectRouter, tenantsRouter],
	});
	await expressRouter.start();
}
start();

process.on('SIGTERM', async () => {
	info(`Received system signal 'SIGTERM'. Shutting down service...`);
	expressRouter.getServer()?.close();
	await mongoDBInterface.disconnect();
});
