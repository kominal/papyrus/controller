import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export interface Translation {
	_id?: string;
	tenantId: string;
	projectName?: string | null;
	type: string;
	key: string;
	values: { language: string; value: string }[];
}

export const TranslationDatabase = model<Document & Translation>(
	'Translation',
	new Schema(
		{
			tenantId: { type: Types.ObjectId, ref: 'Tenant' },
			projectName: String,
			type: String,
			key: String,
			values: [Schema.Types.Mixed],
		},
		{ minimize: false }
	).index({ tenantId: 1, projectName: 1, key: 1 }, { unique: true })
);
