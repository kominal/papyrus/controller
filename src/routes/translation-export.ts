import { Router } from '@kominal/lib-node-express-router';
import { PaginationRequest } from '@kominal/lib-node-mongodb-interface';
import { TranslationDatabase } from '../models/translation';

export const translationExportRouter = new Router();

translationExportRouter.getAsUser<never, any, PaginationRequest>('/translationsExports', async (req, res, userId) => {
	const translations = await TranslationDatabase.find().lean();

	res.setHeader('Content-disposition', 'attachment; filename=translations.json');

	return {
		statusCode: 200,
		responseBody: translations.map(({ tenantId, projectName, type, key, values }) => {
			return {
				projectName,
				type,
				key,
				values,
			};
		}),
	};
});
