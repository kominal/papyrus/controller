import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse, Types } from '@kominal/lib-node-mongodb-interface';
import { TenantDatabase } from '../models/tenant';
import { Translation, TranslationDatabase } from '../models/translation';

export let translationCache: { tenantId: string; projectName: string; language: string; responseBody: {} }[] = [];

export const translationRouter = new Router();

translationRouter.getAsGuest<{ tenantId: string; projectName: string; language: string }, any, never>(
	'/tenants/:tenantId/translations/projects/:projectName/languages/:language',
	async (req) => {
		const { tenantId, projectName, language } = req.params;

		const cache = translationCache.find((c) => {
			c.tenantId === tenantId && c.projectName === projectName && c.language === language;
		});

		if (cache) {
			return { statusCode: 200, responseBody: cache.responseBody };
		}

		const translations: { values: { value: string }; key: string }[] = await TranslationDatabase.aggregate([
			{ $unwind: '$values' },
			{
				$match: {
					tenantId: Types.ObjectId(tenantId),
					projectName,
					'values.language': language,
				},
			},
		]);
		const keys = translations.map((t) => t.key);
		const globalTransactions: { values: { value: string }; key: string }[] = await TranslationDatabase.aggregate([
			{ $unwind: '$values' },
			{
				$match: {
					tenantId: Types.ObjectId(tenantId),
					projectName: null,
					'values.language': language,
					key: { $nin: keys },
				},
			},
		]);

		const responseBody: any = {};

		for (const translation of [...translations, ...globalTransactions]) {
			const parts = translation.key.split('.');

			let step = responseBody;
			for (let index = 0; index < parts.length; index++) {
				const part = parts[index];
				if (index === parts.length - 1) {
					step[part] = translation.values.value || translation.key;
				} else if (!step[part] || typeof step[part] === 'string') {
					step[part] = {};
				}
				step = step[part];
			}
		}

		translationCache.push({
			tenantId,
			projectName,
			language,
			responseBody,
		});

		return { statusCode: 200, responseBody };
	}
);

translationRouter.getAsGuest<{ tenantId: string; projectName: string; language: string; key: string }, { translation: string }, never>(
	'/tenants/:tenantId/translations/projects/:projectName/languages/:language/keys/:key',
	async (req) => {
		let translation = '';

		const projectTranslation = await TranslationDatabase.findOne({
			tenantId: req.params.tenantId,
			projectName: req.params.projectName,
			key: req.params.key,
		});

		if (projectTranslation) {
			translation = projectTranslation.values.find((t) => t.language === req.params.language)?.value || '';
		} else {
			const globalTranslation = await TranslationDatabase.findOne({
				tenantId: req.params.tenantId,
				projectName: null,
				key: req.params.key,
			});

			if (globalTranslation) {
				translation = globalTranslation.values.find((t) => t.language === req.params.language)?.value || '';
			} else {
				await TranslationDatabase.create({
					tenantId: req.params.tenantId,
					projectName: req.params.projectName,
					key: req.params.key,
					type: 'SINGLE',
					values: [],
				});
			}
		}

		return {
			statusCode: 200,
			responseBody: {
				translation,
			},
		};
	}
);

translationRouter.getAsUser<{ tenantId: string }, PaginationResponse<Translation>, PaginationRequest>(
	'/tenants/:tenantId/translations',
	async (req) => {
		const responseBody = await applyPagination<Translation>(TranslationDatabase, req.query, {
			filter: {
				tenantId: Types.ObjectId(req.params.tenantId),
			},
		});

		return {
			statusCode: 200,
			responseBody,
		};
	}
);

translationRouter.getAsUser<{ translationId: string }, Translation, never>(
	'/tenants/:tenantId/translations/:translationId',
	async (req) => {
		return {
			statusCode: 200,
			responseBody: await TranslationDatabase.findOne({ _id: req.params.translationId })
				.lean()
				.orFail(new Error('transl.error.notFound.translation')),
		};
	}
);

translationRouter.postAsUser<{ tenantId: string }, Translation & { tenantId: never }, { _id: string }, never>(
	'/tenants/:tenantId/translations',
	async (req) => {
		delete req.body._id;
		delete req.body.tenantId;

		const translation = await TranslationDatabase.create({
			...req.body,
			tenantId: req.params.tenantId,
		});
		translationCache = [];

		return {
			statusCode: 200,
			responseBody: {
				_id: translation._id,
			},
		};
	}
);

translationRouter.postAsGuest<{ tenantId: string; projectName: string }, { language: string; key: string }[], never, never>(
	'/tenants/:tenantId/translations/projects/:projectName/missing',
	async (req) => {
		try {
			if (!TenantDatabase.exists({ _id: req.params.tenantId })) {
				throw new Error('transl.error.tenant.notFound');
			}

			for (const { key } of req.body) {
				if (
					!(await TranslationDatabase.exists({
						tenantId: req.params.tenantId,
						projectName: req.params.projectName,
						key,
					}))
				) {
					await TranslationDatabase.create({
						tenantId: req.params.tenantId,
						projectName: req.params.projectName,
						key,
						type: 'SINGLE',
						values: [],
					});
				}
			}
		} catch (e) {
			console.log(e);
		}
		translationCache = [];

		return {
			statusCode: 200,
		};
	}
);

translationRouter.putAsUser<{ tenantId: string; translationId: string }, Translation & { tenantId: never }, never, never>(
	'/tenants/:tenantId/translations/:translationId',
	async (req) => {
		delete req.body.tenantId;
		await TranslationDatabase.updateMany({ tenantId: req.params.tenantId, _id: req.params.translationId }, req.body);
		translationCache = [];
		return {
			statusCode: 200,
		};
	}
);

translationRouter.deleteAsUser<{ tenantId: string; translationId: string }, never, never>(
	'/tenants/:tenantId/translations/:translationId',
	async (req) => {
		await TranslationDatabase.deleteMany({ tenantId: req.params.tenantId, _id: req.params.translationId });
		translationCache = [];
		return {
			statusCode: 200,
		};
	}
);
